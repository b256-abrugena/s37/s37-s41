// Set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require ("cors");

// Set up server
const.app = express()

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection via Mongoose
mongoose.connect("mongodb+srv://admin:admin1234@b256abrugena.rckx5fh.mongodb.net/B256_CourseAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`We're connected to the cloud database`));

// Server listening
app.listen(4000, () => console.log(`API is now online on port 4000`));